package com.example.crudApi.organization.entity;
import javax.persistence.*;

import lombok.*;


@Entity
@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@Table(name = "organization")
public class Organization {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)  
    @Column(name = "id")  
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "industry")
    private String industry;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "userId")
    private long managedByUser;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getManagedByUser() {
        return managedByUser;
    }

    public void setManagedByUser(long managedByUser) {
        this.managedByUser = managedByUser;
    }

  
}
