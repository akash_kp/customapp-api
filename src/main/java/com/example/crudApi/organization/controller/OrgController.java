package com.example.crudApi.organization.controller;

import java.util.List;

import com.example.crudApi.organization.entity.Organization;
import com.example.crudApi.organization.repository.OrganizationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/org")
public class OrgController {

    @Autowired
    OrganizationRepository organizationRepository;
    
    @PostMapping("/saveOrg")
    public Organization saveOrganization(@RequestBody Organization organization){
        return this.organizationRepository.save(organization);
    }
    
    @GetMapping("/getOrg/{userId}")
    public List<Organization> getAllOrganization(@PathVariable long userId){
        return this.organizationRepository.findAllByManagedByUser(userId);
    }
    
}
