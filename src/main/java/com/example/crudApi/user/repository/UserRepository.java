package com.example.crudApi.user.repository;

import com.example.crudApi.user.entity.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
    User findByUserEmail(String userEmail);
}
