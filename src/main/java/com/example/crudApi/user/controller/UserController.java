package com.example.crudApi.user.controller;

import com.example.crudApi.user.repository.UserRepository;
import com.example.crudApi.user.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserRepository userRepository;
    @PostMapping("/saveUser")
    public ResponseEntity<Object> saveUser(@RequestBody User user){
        if (this.userRepository.findByUserEmail(user.getUserEmail()) != null){
            return  new ResponseEntity<>("email id already exisist", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        System.out.print(user);
        this.userRepository.save(user);
        return  new ResponseEntity<>(true, HttpStatus.OK);
    }
    
    @GetMapping("/getUser")
    public ResponseEntity<Object> getUser(@RequestHeader String email){
        User user =  this.userRepository.findByUserEmail(email);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PostMapping("/logIn")
    public ResponseEntity<Object> logIn(@RequestHeader String email, @RequestHeader String password){
        User user =  this.userRepository.findByUserEmail(email);
        if(user == null){
            return new ResponseEntity<>("user not registerd", HttpStatus.INTERNAL_SERVER_ERROR);
        }else if(!password.equals(user.getPassword())){
            return new ResponseEntity<>("incorrect password", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

}